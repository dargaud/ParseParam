// ParseParam.h -- See ParseParam.c for example of use

#ifndef _PARSE_PARAM
#define _PARSE_PARAM

#include <stdlib.h>
#include <iso646.h>

extern char* ReadParseParam(const char* FileName, char *VariableName);

extern char *TempPP;

// Changeable by the calling program
extern int ParseParam_ShowErrors;	// 0 for none, 1 for stdout, 2 for message popups
extern int ParseParam_ErrorLevel;	// 0 for none, 1 for file errors, 2 also for syntax errors, 3 also for variables not found
extern int ParseParam_DefaultToZero;// If set, variables not found in the file are set to 0 or ""

#define ParseParamString(FileName, Str) \
	{ if ((TempPP=ReadParseParam((FileName), #Str))!=NULL) \
		strcpy(Str, TempPP); else if (ParseParam_DefaultToZero) Str[0]='\0'; }
		
#define ParseParamInt(FileName, Int) \
	{ if ((TempPP=ReadParseParam((FileName), #Int))!=NULL) \
		Int=atoi(TempPP); else if (ParseParam_DefaultToZero) Int=0; }

#define ParseParamHex(FileName, Int) \
	{ if ((TempPP=ReadParseParam((FileName), #Int))!=NULL) \
		Int=strtol(TempPP, NULL, 16); else if (ParseParam_DefaultToZero) Int=0; }

#define ParseParamFloat(FileName, Flt) \
	{ if ((TempPP=ReadParseParam((FileName), #Flt))!=NULL) \
		Flt=atof(TempPP); else if (ParseParam_DefaultToZero) Flt=0; }

#define ParseParamBool(FileName, B) \
	{ if ((TempPP=ReadParseParam((FileName), #B))!=NULL) \
		B=(toupper(TempPP[0])=='Y' || toupper(TempPP[0])=='T'|| TempPP[0]=='1'); \
		else if (ParseParam_DefaultToZero) B=0; }


#endif
